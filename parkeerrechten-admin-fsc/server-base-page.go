package main

type PagePath string

const (
	PagePathHome     PagePath = "/"
	PagePathRetrieve PagePath = "/ophalen"
	PagePathAdd      PagePath = "/toevoegen"
)

type basePage struct {
	SelfOrganizationName string
	ActivePagePath       PagePath
	PagePathHome         PagePath
	PagePathRetrieve     PagePath
	PagePathAdd          PagePath
}
