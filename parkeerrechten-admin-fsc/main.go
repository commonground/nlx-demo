package main

import (
	"embed"
	"log"
	"os"
	"path/filepath"

	"github.com/caarlos0/env"
)

//go:embed templates/**
var tplFolder embed.FS

type config struct {
	ListenAddress    string   `env:"LISTEN_ADDRESS" envDefault:":8081"`
	OrganizationName string   `env:"ORGANIZATION_NAME" envDefault:"Vergunningsoftware BV"`
	OutwayAddress    string   `env:"OUTWAY_ADDRESS" envDefault:"http://localhost:7605"`
	Municipalities   []string `env:"MUNICIPALITIES" envDefault:"Stijns"`
}

func main() {
	cfg, err := parseEnvVariables()
	if err != nil {
		log.Fatalf("failed to parse env variables: %s", err)
	}

	workDir, err := os.Getwd()
	if err != nil {
		log.Fatalf("failed to get work dir", err)
	}

	staticFilesPath := filepath.Join(workDir, "static")

	server, err := NewServer(&NewServerArgs{
		StaticAssetsPath: staticFilesPath,
		OrganizationName: cfg.OrganizationName,
		OutwayAddress:    cfg.OutwayAddress,
		Municipalities:   cfg.Municipalities,
	})
	if err != nil {
		log.Fatalf("failed to create server: %s", err)
	}

	log.Printf("started listening on %s\n", cfg.ListenAddress)

	err = server.ListenAndServe(cfg.ListenAddress)
	if err != nil {
		log.Fatalf("failed to start server: %s", err)
	}
}

func parseEnvVariables() (config, error) {
	cfg := config{}

	err := env.Parse(&cfg)
	if err != nil {
		return cfg, err
	}

	return cfg, nil
}
