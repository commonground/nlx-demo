package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
	"time"
)

type Server struct {
	staticAssetsPath     string
	httpServer           *http.Server
	outwayAddress        string
	selfOrganizationName string
	municipalities       []string
}

type NewServerArgs struct {
	StaticAssetsPath string
	OrganizationName string
	OutwayAddress    string
	Municipalities   []string
}

func NewServer(args *NewServerArgs) (*Server, error) {
	if args.OrganizationName == "" {
		return nil, fmt.Errorf("organization name must be set")
	}

	if args.StaticAssetsPath == "" {
		return nil, fmt.Errorf("static assets path must be set")
	}

	if args.OutwayAddress == "" {
		return nil, fmt.Errorf("outway address must be set")
	}

	if len(args.Municipalities) == 0 {
		return nil, fmt.Errorf("at least one municipality must be provided")
	}

	server := &Server{
		selfOrganizationName: args.OrganizationName,
		staticAssetsPath:     args.StaticAssetsPath,
		outwayAddress:        args.OutwayAddress,
		municipalities:       args.Municipalities,
	}

	return server, nil
}

func (s *Server) ListenAndServe(address string) error {
	const (
		compressionLevel  = 5
		readHeaderTimeout = 5 * time.Second
	)

	staticAssetsDir := http.Dir(s.staticAssetsPath)

	r := chi.NewRouter()
	r.Use(middleware.Compress(compressionLevel))
	r.Use(middleware.Logger)
	r.Get("/", s.handlerHome)
	r.Get("/ophalen", s.handlerRetrieve)
	r.Get("/toevoegen", s.handlerAddGet)
	r.Post("/toevoegen", s.handlerAddPost)
	r.Get("/kenteken", s.handlerKentekenGet)
	r.Handle("/assets/*", http.StripPrefix("/assets/", http.FileServer(staticAssetsDir)))

	s.httpServer = &http.Server{
		Addr:              address,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	err := s.httpServer.ListenAndServe()
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}
