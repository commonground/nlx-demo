Parkeerrechten Admin FSC
---

1. Start the `parkeerrechten-api` using Docker Compose
1. Start the `kenteken-api` using Docker Compose
2. Run the development environment for NLX FSC
3. Add the 'parkeerrechten' service to RvRD (Organization B)
Name: `parkeerrechten`
Endpoint URL: `http://localhost:8000`
Inway: `<use-default-inway-address>`
4. Add the 'basisregister-fictieve-kentekens' service to RvRD (Organization B)
Name: `basisregister-fictieve-kentekens`
Endpoint URL: `http://localhost:8010`
Inway: `<use-default-inway-address>`
4. Request & approve access to both services by using the `demo.http` requests from the `admin` component
5. Open the Parkeerrechten Admin `http://localhost:8081`
5. Use the Grant Hash of the ServiceConnectionGrant to load the kenteken & parkeerrechten data in the demo

```shell
modd
```
