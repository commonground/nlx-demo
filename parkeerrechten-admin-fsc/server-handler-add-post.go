package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
)

func (s *Server) handlerAddPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to parse form: %s", err), http.StatusInternalServerError)
	}

	err = submitParkeerrecht(
		s.outwayAddress,
		r.FormValue("parkeerrechtenGrantHash"),
		r.FormValue("license"),
		r.FormValue("year"),
		r.FormValue("municipality"),
	)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to submit parkeerrecht: %s", err), http.StatusInternalServerError)

		return
	}

	p := addPage{
		basePage: &basePage{
			SelfOrganizationName: s.selfOrganizationName,
			ActivePagePath:       PagePathAdd,
			PagePathHome:         PagePathHome,
			PagePathRetrieve:     PagePathRetrieve,
			PagePathAdd:          PagePathAdd,
		},
		Municipalities: s.municipalities,
		Licenses: []string{
			"RT774D",
			"KN958B",
			"81HZFB",
			"GJ713R",
			"50HSZS",
			"KS98DN",
		},
		SuccessMessage: "Parkeerrecht is toegevoegd.",
		Form: &addPageForm{
			ParkeerrechtenGrantHash: r.FormValue("parkeerrechtenGrantHash"),
			KentekenGrantHash:       r.FormValue("kentekenGrantHash"),
			Municipality:            r.FormValue("municipality"),
			GrantHash:               r.FormValue("grantHash"),
			License:                 r.FormValue("license"),
			Year:                    r.FormValue("year"),
		},
	}

	t, err := template.ParseFS(
		tplFolder,
		"templates/base.html",
		"templates/add.html",
	)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to parse template: %s", err), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base.html", p)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to render template: %s", err), http.StatusInternalServerError)

		return
	}
}

func submitParkeerrecht(outwayBaseURL, grantHash string, kenteken, jaartalGeldigheidrecht, gemeenteGeldigheidrecht string) error {
	values := map[string]string{
		"kenteken":                kenteken,
		"jaartalGeldigheidrecht":  jaartalGeldigheidrecht,
		"gemeenteGeldigheidrecht": gemeenteGeldigheidrecht,
	}

	jsonData, err := json.Marshal(values)
	if err != nil {
		return fmt.Errorf("failed to marshal json: %s", err)
	}

	postParkeerrechtURL := fmt.Sprintf("%s/parkeerrechten/", outwayBaseURL)

	req, err := http.NewRequest(http.MethodPost, postParkeerrechtURL, bytes.NewBuffer(jsonData))
	if err != nil {
		return fmt.Errorf("client: could not create request to outway %s: %s", outwayBaseURL, err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Fsc-Grant-Hash", grantHash)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("client: error making http request to outway %s: %s", outwayBaseURL, err)
	}

	if res.StatusCode != http.StatusCreated {
		var data []byte
		data, err = io.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("client: could not read response body: %s\n", err)
		}

		return fmt.Errorf("client: error making http request to outway %s: %s", outwayBaseURL, data)
	}

	return nil
}
