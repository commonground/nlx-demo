from django.db import models


class Parkeerrecht(models.Model):
    kenteken = models.CharField(max_length=6)
    jaartalGeldigheidrecht = models.PositiveIntegerField('jaartal geldigheidrecht')
    gemeenteGeldigheidrecht = models.CharField(max_length=100)
