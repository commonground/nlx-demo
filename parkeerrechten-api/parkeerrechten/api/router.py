from rest_framework import routers
from parkeerrechten.api import views

router = routers.DefaultRouter()
router.register(r'parkeerrechten', views.ParkeerrechtViewSet)
