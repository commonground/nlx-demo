Parkeerrechten API
------------------

## Setup your development environment

### Virtualenv and packages

Make sure you created a virtual environment, using:

```
python3 -m venv .venv
```

Then activate the virtual environment:

```
source .venv/bin/activate
```

Then install the requirements based on the environment:

```
pip install -r requirements/development.txt
```


### Database

Run migrations using `manage.py`:

```
python3 manage.py migrate
```

The application uses SQLite to store its data.
The database wil be created at `./db.sqlite3` if it does
not exist yet.

Fixtures can also be added using `manage.py`:

```
python3 manage.py loaddata parkeerrechten/fixtures/data.json
```

### Development server

You can run the project using the Django development webserver using:

```
python3 manage.py runserver
```

## Docker

### Docker

There is a Docker Compose file provided for development purposes.
Start the API using the following command:

```
docker-compose up
```

Note: to load the fixtures, uncomment the `fixtures` job in the Docker Compose file.
