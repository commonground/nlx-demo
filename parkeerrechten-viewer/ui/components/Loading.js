// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'

const Flex = styled.div`
  display: flex;
  justify-content: center;
  margin-top: ${(p) => p.theme.tokens.spacing09};
`

const Item = styled.span`
  margin: 0 ${(p) => p.theme.tokens.spacing02};
  animation: loading 1s 0s infinite;
  transform: scale(1);

  &:nth-child(1) {
    animation-delay: 0s;
  }

  &:nth-child(2) {
    animation-delay: 0.3s;
  }

  &:nth-child(3) {
    animation-delay: 0.6s;
  }

  @keyframes loading {
    30% {
      transform: scale(1);
    }
    50% {
      transform: scale(2);
    }
    70% {
      transform: scale(1);
    }
  }
`

const Loading = () => (
  <Flex>
    <Item>🚗</Item>
    <Item>🅿️</Item>
    <Item>🚙</Item>
  </Flex>
)

export default Loading
