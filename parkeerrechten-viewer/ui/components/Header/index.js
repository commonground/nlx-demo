// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { PrimaryNavigation } from '@commonground/design-system'
import getConfig from 'next/config'
import { useRouter } from 'next/router'
import NavLink from 'ui/components/NavLink'

const { publicRuntimeConfig } = getConfig()

const LogoWrapper = styled.div`
  padding: ${(p) => p.theme.tokens.spacing05};
`

const Logo = styled.img`
  width: 200px;
`

const Header = () => {
  const { pathname } = useRouter()

  return (
    <header>
      <LogoWrapper>
        <Logo
          src={publicRuntimeConfig.ORGANIZATION_LOGO}
          alt={publicRuntimeConfig.ORGANIZATION_NAME}
        />
      </LogoWrapper>

      <PrimaryNavigation
        LinkComponent={NavLink}
        pathname={pathname}
        mobileMoreText="Meer"
        items={[
          {
            name: 'Parkeerrechten',
            to: '/',
          },
        ]}
      />
    </header>
  )
}

export default Header
