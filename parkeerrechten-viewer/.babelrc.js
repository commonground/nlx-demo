// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
const { extendDefaultPlugins } = require('svgo')

module.exports = {
  presets: ['next/babel'],
  plugins: [
    [
      'styled-components',
      {
        ssr: true
      }
    ],
    [
      'inline-react-svg',
      {
        caseSensitive: true,
        svgo: {
          plugins: extendDefaultPlugins([
            {
              name: 'cleanupIDs',
              params: {
                minify: false,
                preserve: [
                  'dashedlines'
                ]
              }
            },
            'removeDimensions'
          ])
        }
      }
    ]
  ]
}
