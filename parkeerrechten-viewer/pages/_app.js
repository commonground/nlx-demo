// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { ThemeProvider } from 'styled-components'
import Head from 'next/head'
import { GlobalStyles } from '@commonground/design-system'
import theme from 'ui/styling/theme'
import 'ui/styling/fonts.css'

function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <GlobalStyles />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default App
