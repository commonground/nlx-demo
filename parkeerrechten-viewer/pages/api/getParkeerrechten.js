// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import getConfig from 'next/config'

const { serverRuntimeConfig } = getConfig()
const baseUrl = serverRuntimeConfig.PARKEERRECHTEN_API_BASE_URL

export default async function handler(req, res) {
  try {
    const response = await fetch(`${baseUrl}/parkeerrechten/`)

    if (response.ok && response.status === 200) {
      const json = await response.json()
      res.status(200).json(json)
    } else {
      throw new Error(
        `failed to fetch parkeerrechten. http status: ${response.status}, http status text: ${response.statusText}`,
      )
    }
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
}
