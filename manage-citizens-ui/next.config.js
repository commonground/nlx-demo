// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
module.exports = {
  serverRuntimeConfig: {
    BASISREGISTER_FICTIEVE_PERSONEN_API_BASE_URL:
      process.env.BASISREGISTER_FICTIEVE_PERSONEN_API_BASE_URL,
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
  },
  publicRuntimeConfig: {
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
    ORGANIZATION_LOGO: process.env.ORGANIZATION_LOGO,
  },
}
