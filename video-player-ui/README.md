# Video Player UI

This app is used by the demo municipalities (eg. Stijns) to demonstrate that video streaming works with NLX.

## Development

The application expects the video being served from an Outway. 
Specify the Outway address including the organization and service via the variable `OUTWAY_PROXY_URL`.

Example: `OUTWAY_PROXY_URL=http://my-outway/<organization>/<service>/`

The organization name can be set using `ORGANIZATION_NAME`.

Run the development server of this app with:

```shell
npm install
npm start
```
