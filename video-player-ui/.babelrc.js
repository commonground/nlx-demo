// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

module.exports = {
  presets: ['next/babel'],
  plugins: [
    [
      'styled-components',
      {
        ssr: true
      }
    ]
  ]
}
