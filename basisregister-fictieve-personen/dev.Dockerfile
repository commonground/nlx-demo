FROM golang:1.20-alpine

WORKDIR /api

COPY . .

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

ENTRYPOINT CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.c|.+\.sql|.+\.toml)$" -exclude-dir=.git -build="go build -o api ." -command="./api serve"
