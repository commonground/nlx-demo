// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package seeder

import (
	"context"
	_ "embed"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/api"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/database"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/database/queries"
)

const ()

//go:embed gemeente-stijns.json
var stijns []byte

//go:embed gemeente-riemer.json
var riemer []byte

func Seed(db *database.Database, municipality string) error {
	p, err := db.Queries.ListPersonen(context.TODO())
	if err != nil {
		return err
	}

	if len(p) > 0 {
		return nil
	}

	res := make(map[string]api.Persoon)

	var data []byte
	switch municipality {
	case "gemeente-stijns":
		data = stijns
	case "gemeente-riemer":
		data = riemer
	default:
		return fmt.Errorf("unknown municipality: %q", municipality)
	}

	err = json.Unmarshal(data, &res)
	if err != nil {
		return err
	}

	for _, p := range res {
		data, err := json.Marshal(p)
		if err != nil {
			return err
		}

		parsedID, err := uuid.Parse(p.Identificatie)
		if err != nil {
			return err
		}

		err = db.Queries.CreatePersoon(context.TODO(), &queries.CreatePersoonParams{
			Identificatie: parsedID,
			Data:          data,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
