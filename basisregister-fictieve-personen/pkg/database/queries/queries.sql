-- name: ListPersonen :many
SELECT identificatie,
    data
FROM nlx_demo_brp.personen;

-- name: CreatePersoon :exec
INSERT INTO nlx_demo_brp.personen (
    identificatie,
    data
)
VALUES ($1, $2);

-- name: GetPersoon :one
SELECT identificatie,
    data
FROM nlx_demo_brp.personen
WHERE identificatie = $1;

-- name: GetPersoonByBsn :one
SELECT identificatie,
    data
FROM nlx_demo_brp.personen
WHERE data ->> 'burgerservicenummer' = $1;


-- name: UpdatePersoon :exec
UPDATE nlx_demo_brp.personen
SET data = $1
WHERE identificatie = $2;

-- name: DeletePersoon :exec
DELETE FROM nlx_demo_brp.personen
WHERE identificatie = $1;

-- name: DeregisterPersoon :exec
UPDATE nlx_demo_brp.personen
SET data = jsonb_set(data::jsonb, '{verblijfadres}', '{}')
WHERE identificatie = $1;

-- name: RegisterPersoon :exec
UPDATE nlx_demo_brp.personen
SET data = jsonb_set(data::jsonb, '{verblijfadres}', sqlc.arg(address))
WHERE identificatie = $1;
