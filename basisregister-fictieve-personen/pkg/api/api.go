// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package api

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/database"
)

type API struct {
	logger *zap.Logger
	server *http.Server
	db     *database.Database
	mux    *chi.Mux
}

type NewAPIArgs struct {
	DB            *database.Database
	Logger        *zap.Logger
	ListenAddress string
}

func New(args *NewAPIArgs) (*API, error) {
	a := &API{
		logger: args.Logger,
		db:     args.DB,
	}

	a.mux = createRouter(a)

	a.server = &http.Server{
		Addr: args.ListenAddress,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			a.logger.Info("Incoming request", zap.String("path", r.URL.Path))
			a.ServeHTTP(w, r)
		}),
	}

	return a, nil
}

func (a *API) ListenAndServe() error {
	return a.server.ListenAndServe()
}

func (a *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.mux.ServeHTTP(w, r)
}
