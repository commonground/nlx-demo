// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

const Home = ({ children }) => {
  return (
    <div style={{ paddingTop: 50 }}>
      <p>
        Dit is een voorbeeldapplicatie voor het beheren van parkeerrechten door
        een SaaS-leverancier voor verschillende gemeenten. De data loopt via
        NLX.
      </p>

      <p>
        Er zijn verschillende manieren mogelijk om de parkeerrechten op te
        halen. De flow is steeds als volgt:
      </p>
      <p>Client (deze website) → API → Outway → Inway → Parkeerrechten (API)</p>

      {children}
    </div>
  )
}

export default Home
