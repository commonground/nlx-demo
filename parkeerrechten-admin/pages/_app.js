// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import Head from 'next/head'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles, defaultTheme } from '@commonground/design-system'
import '@fontsource/source-sans-pro/latin.css'
import getConfig from 'next/config'
import Layout from '../components/layout'
const { publicRuntimeConfig } = getConfig()

const App = ({ Component, pageProps }) => {
  const organizationName = publicRuntimeConfig.ORGANIZATION_NAME

  return (
    <ThemeProvider theme={defaultTheme}>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title>{organizationName} - NLX demo</title>
      </Head>
      <GlobalStyles />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  )
}

export default App
