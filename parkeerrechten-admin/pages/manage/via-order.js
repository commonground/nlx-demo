// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import AccessViaOrder from '../../components/access-via-order'

const ViaOrder = ({ children }) => {
  return (
    <>
      <h2>Toegang via een opdracht</h2>
      <p>
        Vergunningsoftware BV heeft zelf geen directe toegang tot een service.
        Via een opdracht van de gemeente heeft hij gedelegeerde toegang.
      </p>
      <p>
        Om de opdracht te kunnen gebruiken moet je het referentienummer en de
        delegator meegeven.
      </p>

      <AccessViaOrder />

      {children}
    </>
  )
}

export default ViaOrder
