// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()

const Container = styled.div`
  margin: 10vh auto;
  width: 650px;
  max-width: 100%;
`

const Layout = ({ children }) => {
  const organizationName = publicRuntimeConfig.ORGANIZATION_NAME

  return (
    <Container>
      <h1>{organizationName} — Parkeerrechten</h1>
      {children}
    </Container>
  )
}

export default Layout
