// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import { useState } from 'react'
import {
  Alert,
  Button,
  Fieldset,
  Legend,
  Label,
} from '@commonground/design-system'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import fetchJson from '../data/fetch-json-utility'
import Add from './add'

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const Th = styled.th`
  text-align: left;
`

const Td = styled.td`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`

const defaultBearerToken =
  '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'

const AccessViaOrder = () => {
  const [bearerToken, setBearerToken] = useState(defaultBearerToken)
  const [orderReference, setOrderReference] = useState('')
  const [orderDelegator, setOrderDelegator] = useState('')
  const [parkeerrechten, setParkeerrechten] = useState([])
  const [error, setError] = useState()

  const fetchData = async () => {
    setParkeerrechten([])
    setError(null)

    try {
      const headers = {}

      if (bearerToken && bearerToken.length > 0) {
        headers['x-nlx-authorization'] = `Bearer ${bearerToken}`
      }

      if (orderDelegator && orderDelegator.length > 0) {
        headers['x-nlx-request-delegator'] = orderDelegator
      }

      if (orderReference && orderReference.length > 0) {
        headers['x-nlx-request-order-reference'] = orderReference
      }

      const data = await fetchJson('/api/getParkeerrechten', {
        headers: headers,
      })

      setParkeerrechten(data)
      setError(null)
    } catch (error) {
      setParkeerrechten([])
      setError(error.message)
    }
  }

  return (
    <>
      {error && (
        <Alert variant="error" title="Unexpected error">
          {error}
        </Alert>
      )}

      <form>
        <Fieldset>
          <Legend>Specificaties van de opdracht</Legend>

          <Label>Referentie</Label>
          <small>Bijvoorbeeld &apos;parkeerrechtendemo&apos;</small>
          <Input
            placeholder="Referentie"
            size="m"
            onChange={(e) => setOrderReference(e.target.value)}
          />

          <Label>Delegatiegever</Label>
          <small>OIN van de gemeente Stijns (12345678901234567890)</small>
          <Input
            placeholder="Delegatiegever"
            size="m"
            onChange={(e) => setOrderDelegator(e.target.value)}
          />
        </Fieldset>

        <Fieldset>
          <Legend>Outway authorisatie (via OPA)</Legend>

          <p>
            Om specifieker de toegang te kunnen regelen kan je authorizatie
            uitvoeren op de Outway. In dit voorbeeld maakt de Outway gebruik van
            Open Policy Agent (OPA). Hiermee kan een verfijning op de toegang op
            organisatieniveau ingezet worden. In deze applicatie hebben we een
            tweetal voorbeeld gebruikers toegevoegd die toegang hebben.
          </p>

          <Label>Bearer token</Label>
          <small>
            <a
              href="https://gitlab.com/commonground/nlx/nlx/-/blob/master/auth-opa/outway/data/users.json"
              target="_blank"
              rel="nofollow noreferrer"
            >
              Lijst van alle beschikbare tokens op GitLab
            </a>
          </small>
          <Input
            placeholder="Bearer token"
            size="m"
            defaultValue={defaultBearerToken}
            onChange={(e) => setBearerToken(e.target.value)}
          />
        </Fieldset>
      </form>

      <Table>
        <thead>
          <tr>
            <Th>#</Th>
            <Th>Kenteken</Th>
            <Th>Geldig in</Th>
            <Th>Gemeente</Th>
          </tr>
        </thead>

        <tbody>
          {parkeerrechten.length ? (
            parkeerrechten.map((parkeerrecht, i) => (
              <tr key={i}>
                <Td>{i + 1}</Td>
                <Td>{parkeerrecht.kenteken}</Td>
                <Td>{parkeerrecht.jaartalGeldigheidrecht}</Td>
                <Td>{parkeerrecht.gemeenteGeldigheidrecht}</Td>
              </tr>
            ))
          ) : (
            <tr>
              <Td colSpan="4">Geen parkeerrechten gevonden</Td>
            </tr>
          )}
        </tbody>
      </Table>

      <br />

      <Button type="button" onClick={fetchData}>
        Opnieuw ophalen
      </Button>

      <Add
        orderReference={orderReference}
        orderDelegator={orderDelegator}
        bearerToken={bearerToken}
      />
    </>
  )
}

export default AccessViaOrder
