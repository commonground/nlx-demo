// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import generateOrderHeaders from './orderinfo-util'

const fetchLicenceRelatedData =
  (orderInfo) =>
  async (
    kenteken,
    refFetchController
  ) => {
    if (refFetchController) {
      refFetchController.current.abort()
      refFetchController.current = new AbortController()
    }

    const response = await fetch(`/api/getDataByKenteken?k=${kenteken}`, {
      headers: generateOrderHeaders(orderInfo),
      signal: refFetchController?.current?.signal,
    })

    return await response.json()
  }

export default fetchLicenceRelatedData
