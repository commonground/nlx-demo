// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

const generateOrderHeaders = (orderInfo) => {
  if (!orderInfo.filled) return {}
  return {
    'X-NLX-Request-Order-Reference': orderInfo.reference,
    'X-NLX-Request-Delegator': orderInfo.delegator,
  }
}

export default generateOrderHeaders
