import json

from flask import Flask, Response, jsonify
from flask_cors import cross_origin
from flask_restful import Api

from schema import spec, VoertuigLijstSchema, VoertuigSchema


app = Flask(__name__)
api = Api(app)

with open('data.json', 'r') as f:
    data = json.load(f)


@app.after_request
def apply_security_headers(response):
    response.headers['X-Frame-Options'] = 'DENY'
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['X-XSS-Protection'] = '1; mode=block'
    response.headers['Referrer-Policy'] = 'same-origin'
    response.headers['Content-Security-Policy'] = "default-src 'self'; style-src 'self' 'unsafe-inline'"
    response.headers['Cache-Control'] = 'max-age=0, no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    return response

@app.route('/', methods=['GET'])
@cross_origin()
def get_index():
    return jsonify({
        'bericht': 'Dit is het Basisregister Fictieve Kentekens (BFK). Voor de API specificatie bekijk /openapi.json. '
                   'Voer een GET uit op /voertuigen voor een lijst van voertuigen.'
    })

@app.route('/voertuigen', methods=['GET'])
@cross_origin()
def get_voertuigen():
    """VoertuigLijst
    ---
    get:
      description: Toon een lijst met voertuigen
      responses:
        200:
          content:
            application/json:
              schema: VoertuigLijstSchema
    """
    body = {
        'aantal': len(data),
        'resultaten': data
    }

    return jsonify(VoertuigLijstSchema().dump(body))

@app.route('/voertuig/<string:kenteken>', methods=['GET'])
@cross_origin()
def get_voertuig_by_kenteken(kenteken):
    """Voertuiginformatie
    ---
    get:
      description: Zoek een voertuig op kenteken
      responses:
        200:
          content:
            application/json:
              schema: VoertuigSchema
    """

    for voertuig in data:
        if voertuig['kenteken'] == kenteken:
            return jsonify(VoertuigSchema().dump(voertuig))

    body = {
        'code': 404,
        'bericht': 'Dit voertuig is niet gevonden.'
    }

    return jsonify(body), 404

@app.route('/openapi.json', methods=['GET'])
@cross_origin()
def openapi_json():
    return jsonify(spec.to_dict())


@app.route('/openapi.yaml', methods=['GET'])
@cross_origin()
def openapi_yaml():
    return Response(spec.to_yaml(), mimetype='application/yaml')


with app.test_request_context():
    spec.path(view=get_voertuigen)
    spec.path(view=get_voertuig_by_kenteken)


if __name__ == '__main__':
    app.run(debug=True, port=5010)  # This debug mode is not executed when in uwsgi mode
