apispec[yaml]==6.3.0
apispec-webframeworks==0.5.2
Flask==2.3.3
Flask-Cors==4.0.0
Flask-RESTful==0.3.10
marshmallow==3.20.1
uWSGI==2.0.22
